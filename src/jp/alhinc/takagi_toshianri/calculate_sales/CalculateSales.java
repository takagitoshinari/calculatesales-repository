package jp.alhinc.takagi_toshianri.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchMap = new HashMap<>();
		Map<String, Long> salesMap = new HashMap<>();

		BufferedReader br = null;
		File file = new File(args[0], "branch.lst");
		if (!(file.exists())) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] separateBranch = line.split(",", 0);
				if (!(separateBranch[0].matches("^[0-9]{3}")) || separateBranch.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchMap.put(separateBranch[0], separateBranch[1]);
				salesMap.put(separateBranch[0], (long) 0);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				if (str.matches("^[0-9]{8}.rcd")) {
					return true;
				} else {
					return false;
				}
			}
		};

		File[] filteringFiles = new File(args[0]).listFiles(filter);
		List<Integer> fileList = new ArrayList<Integer>();

		for (int i = 0; i < filteringFiles.length; i++) {
			String fileName = filteringFiles[i].getName();
			String fileCut = fileName.substring(0, 8);
			int compareFile = Integer.parseInt(fileCut);
			fileList.add(compareFile);
		}
		Collections.sort(fileList);

		for (int i = 0; i < fileList.size() - 1; i++) {
			System.out.println(fileList.get(i + 1));
			if (fileList.get(i + 1) - fileList.get(i) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		
		for (int i = 0; i < filteringFiles.length; ++i) {
			try {
				FileReader fr = new FileReader(filteringFiles[i]);
				br = new BufferedReader(fr);
				String branchCode = br.readLine();
				String banchSales = br.readLine();
				Long banchSales2 = Long.parseLong(banchSales);
				String fileName = filteringFiles[i].getName();

				if (branchMap.containsKey(branchCode)) {
					Long total = banchSales2 + salesMap.get(branchCode);
					salesMap.put(branchCode, total);
				} else {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				if (salesMap.get(branchCode) >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if ((banchSales = br.readLine()) != null) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}
				File outfile = new File(args[0], "branch.out");
				FileWriter fw = new FileWriter(outfile);
				BufferedWriter bw = new BufferedWriter(fw);
				for (Map.Entry<String, String> entry : branchMap.entrySet()) {
					bw.write(entry.getKey() + "," + entry.getValue() + "," + salesMap.get(entry.getKey()) + "\n");
				}
				bw.close();

			} catch (Exception e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
	}

}